
/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: routes.js
 * Objetivo: Módulo responsável por distribuir a rota de requests e response da aplicação
 */

'use strict';

//função responsável por mapear todas as rotas externas da aplicação
module.exports = function(app) {

  //Import de modulos e variaveis importantes
  var path = require( 'path' );
  var regras   = require(path.resolve( 'src/controllers/regrasController' ));
  var calculos = require(path.resolve( 'src/controllers/calculosController' ));
  var dadosDocumento    = require(path.resolve( 'src/controllers/dadosDocumentoController' ));
  var status    = require(path.resolve( 'src/controllers/statusController' ));

  //Mapeamento da rota raiz
  app.route('/')
    .get(status.apresentarStatus);

  //Mapeamento da rota de regras
  app.route('/api/regras')
    .post(regras.processar_regras);

  //Mapeamento da rota de calculos
  app.route('/api/calculos')
    .post(calculos.calcularPreco)
    .put(calculos.aplicarParcelamento);     

  //Mapeamento da rota de consulta e gravação de dados
  app.route('/api/dados')
    .get(dadosDocumento.list_doc)
    .post(dadosDocumento.create_a_doc)
    .put(dadosDocumento.update_a_doc)
    .delete(dadosDocumento.delete_a_doc);
    
};
