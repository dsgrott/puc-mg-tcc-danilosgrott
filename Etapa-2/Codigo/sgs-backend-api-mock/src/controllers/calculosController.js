/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: calculosController.js
 * Objetivo: Modulo responsável por controlar o fluxo de calculos da aplicação
 */

'use strict';

//Imports de modulos obrigatórios
var path = require( 'path' );
var security        = require( path.resolve( 'src/controllers/securityController' ) );
var calculoPrecificao   = require( path.resolve( 'src/controllers/calculos/calculoPrecificacao' ) );
var formaPagamento = require( path.resolve( 'src/controllers/calculos/formaPagamento' ) );
var planoParcelamento = require( path.resolve( 'src/controllers/calculos/planoParcelamento' ) );

//Função responsável por controlar o fluxo de calculo de preço / premio
exports.calcularPreco = function(req, res) {
   
    console.log('Processar Precificacao');

    //Valida segurança MOCK
    security.autorizacao(req,res)

    //Variavel com o JSON de entrada
    var documento = req.body

    //Chama função para validar os status permitidos
    validarStatusPermitidos(req,res, documento)

    //Chama as funçoes de preco e forma de pagamento e altera o status do documento para Calculado
    documento = calculoPrecificao.aplicarPreco(documento)
    documento = formaPagamento.aplicarFormaPagamento(documento)
    documento.status = "Calculado"

    console.log(req.body)

    //Retorno do Json
    res.json(documento);

};

//Função responsável por controlar o fluxo de aplicação de parcelas
exports.aplicarParcelamento = function(req, res) {
   
    console.log('Aplicar Parcelamento');

    //Valida segurança MOCK
    security.autorizacao(req,res)

    //Variavel com o JSON de entrada
    var documento = req.body

    //Chama função para validar os status permitidos
    validarStatusPermitidos(req, res, documento)

    //Chama o fluxo para aplicar parcelas e altera o status para Emitido
    documento =  planoParcelamento.aplicarParcelas(documento)
    documento.status = "Emitido"

    console.log(req.body)

    //Retorno do documento
    res.json(documento);

};

//Função interna para validar os status permitidos
function validarStatusPermitidos(req, res, documento){

        //Valida se o documento esta com status de criticado
        if(documento.criticas.criticado || documento.status == "Criticado"){
            res.status(403).json({ message: 'Documentos criticados não estao autorizados a calcular premio. Corrija e enviei novamente!' });
        }
    
        //Valida se o documento possui algum status não disponivel para o calculo
        if(documento.status != "Validado" && 
           documento.status != "Calculado" ){
        
            res.status(403).json({ message: 'Status não permitido para calcular premio.' });
        }

};

