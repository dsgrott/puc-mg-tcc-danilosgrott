/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: regrasDeNegocio.js
 * Objetivo: Modulo responsável por aplicar as regras de negocio do sistema
 */

'use strict';

const { disable } = require("express/lib/application");

//Função responsável por aplicar o limite de idade
exports.limiteIdade = function(documento) {
    console.log('Validar limite idade Obrigatorio');

    //Variavel de criticas
    var criticas

    //Verifica se a lista de criticas esta nulo, se estiver inicia uma nova, se não apenda os dados existentes
    if(documento.criticas.listaDeCriticas === null){
        criticas = []
    }else{
        criticas = documento.criticas.listaDeCriticas
    }

    console.log("documento.contratante.dataDeNascimento " + documento.contratante.dataDeNascimento)

    //Variaveis para manipulação de data de nascimento
    var dataNascimento = documento.contratante.dataDeNascimento
    var quebraData = dataNascimento.split('-')
    const today = new Date();
    const birthDate = new Date(quebraData[0], quebraData[1], quebraData[2]);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();

    console.log("quebraData " + quebraData)
    console.log("birthDate " + birthDate)

    //If que valida data de nascimento 
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    console.log("Idade " + age)

    //Aplicando regra para idade permitida de emissao de seguro
    if(age <= 17){
        console.log('Aplicando Critica - N001-Idade inferior ao permitido para emissão');
        documento.criticas.criticado=true
        criticas.push("N001-Idade inferior ao permitido para emissão")
    }

    //Add a nova lista de criticas ao documento original
    documento.criticas.listaDeCriticas=criticas

    //retorno da função
    return documento
};

//Função responsável por aplicar o range de antedimento de CEP
exports.rangeAtendimentoCEP = function(documento) {
    console.log('Validar validar de o range de cpf é atendido');

    //Variavel de criticas
    var criticas

    //Verifica se a lista de criticas esta nulo, se estiver inicia uma nova, se não apenda os dados existentes
    if(documento.criticas.listaDeCriticas === null){
        criticas = []
    }else{
        criticas = documento.criticas.listaDeCriticas
    }

    console.log("documento.contratante.cepResidencial " + documento.contratante.cepResidencial)

    //Variaveis para manipulação de CEP
    var cep = documento.contratante.cepResidencial
    var cepQuebrado = cep.split('-')
    var listaCEPBloqueados = ["03575", "03400"]

    console.log("quebraData " + cepQuebrado)
    
    //Aplicando regra para CEP permitido de emissao de seguro
    listaCEPBloqueados.forEach(function(valor){
        if(cepQuebrado[0] == valor){
            console.log('Aplicando Critica - N002-CEP não permitido para emissão');
            documento.criticas.criticado=true
            criticas.push("N001-CEP não permitido para emissão")
        }
    })

    //Add a nova lista de criticas ao documento original
    documento.criticas.listaDeCriticas=criticas

    //Retorno da função
    return documento
};

//Função responsável por validar os limites de coberturas
exports.validarLimites = function(documento) {
    console.log('Validar limites');

    //Variavel de criticas
    var criticas

    //Verifica se a lista de criticas esta nulo, se estiver inicia uma nova, se não apenda os dados existentes
    if(documento.criticas.listaDeCriticas === null){
        criticas = []
    }else{
        criticas = documento.criticas.listaDeCriticas
    }

    console.log("documento.cobertura.BAS "   + documento.cobertura.BAS)
    console.log("documento.cobertura.FURTO " + documento.cobertura.FURTO)   
    console.log("documento.cobertura.MORTE " + documento.cobertura.MORTE)

    //Variaveis para manipulação das coberturas e somas
    var BAS = parseFloat(documento.cobertura.BAS)
    var FURTO = parseFloat(documento.cobertura.FURTO)
    var MORTE = parseFloat(documento.cobertura.MORTE)
    var SOMA = (BAS + FURTO + MORTE)
    var BAS_LIMITE = 50000.00
    var FURTO_LIMITE = 10000.00
    var MORTE_LIMITE = 30000.00
    var LIMITE_TOTAL = 80000.00

    console.log("SOMA " + SOMA)
    console.log("LIMITE_TOTAL " + LIMITE_TOTAL)


    //A partir desse bloco em diante aplicação de criticas para cada item 
    if((BAS>BAS_LIMITE) ){
            console.log('Aplicando Critica - N003-Cobertura BAS ultrapassou o limite ' + BAS_LIMITE);
            documento.criticas.criticado=true
            criticas.push("N003-Cobertura BAS ultrapassou o limite " + BAS_LIMITE)
    }

    if((FURTO>FURTO_LIMITE) ){
        console.log('Aplicando Critica - N004-Cobertura FURTO ultrapassou o limite ' + FURTO_LIMITE);
        documento.criticas.criticado=true
        criticas.push("N004-Cobertura FURTO ultrapassou o limite " + FURTO_LIMITE)
    }

    if((MORTE>MORTE_LIMITE) ){
        console.log('Aplicando Critica - N005-Cobertura MORTE ultrapassou o limite ' + MORTE_LIMITE);
        documento.criticas.criticado=true
        criticas.push("N005-Cobertura MORTE ultrapassou o limite " + MORTE_LIMITE)
    }

    if((SOMA>LIMITE_TOTAL)){
        console.log('Aplicando Critica - N006-Ultrapassou a soma de limites de coberturas ' + LIMITE_TOTAL);
        documento.criticas.criticado=true
        criticas.push("N006-Ultrapassou a soma de limites de coberturas " + LIMITE_TOTAL)
    }

    //Add a nova lista de criticas ao documento original
    documento.criticas.listaDeCriticas=criticas

    //Retorno da função
    return documento
};

