/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: regrasDeCampos.js
 * Objetivo: Modulo responsável por aplicar as regras de campos do sistema
 */

'use strict';

//Função responsável por validar os campos obrigatorios 
exports.validarNullCamposObrigatorios = function(documento) {
    console.log('Validar Null e Campos Obrigatorios');
    
    // Variaveis de criticas
    var criticas

    //Valida se ja não tem criticas no documento, se não tiver cria a lista do zero, se tiver apenda ao documento existe
    if(documento.criticas.listaDeCriticas === null){
        criticas = []
    }else{
        criticas = documento.criticas.listaDeCriticas
    }

    //Desse bloco em diante, ifs de validação de campo, a critica aplicada deixa claro qual regra esta sendo validada
    if(documento.contratante.primeiroNome === null || documento.contratante.primeiroNome === '' || typeof documento.contratante.primeiroNome == "undefined"){
        console.log('Aplicando Critica - C001-Primeiro nome preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C001-Primeiro nome preenchimento obrigatorio")
    }

    if(documento.contratante.sobrenome === null || documento.contratante.sobrenome === '' || typeof documento.contratante.sobrenome == "undefined"){
        console.log('Aplicando Critica - C002-Sobrenome preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C002-Sobrenome preenchimento obrigatorio")
    }
    
    if(documento.contratante.dataDeNascimento === null || documento.contratante.dataDeNascimento === '' || typeof documento.contratante.dataDeNascimento == "undefined"){
        console.log('Aplicando Critica - C003-Data De Nascimento preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C003-Data De Nascimento preenchimento obrigatorio")
    }

    if(documento.contratante.cpf === null || documento.contratante.cpf === '' || typeof documento.contratante.cpf == "undefined"){
        console.log('Aplicando Critica - C004-CPF preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C004-CPF preenchimento obrigatorio")
    }

    if(documento.contratante.telefone === null || documento.contratante.telefone === '' || typeof documento.contratante.telefone == "undefined"){
        console.log('Aplicando Critica - C005-Telefone preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C005-Telefone preenchimento obrigatorio")
    }

    if(documento.contratante.profissao === null || documento.contratante.profissao === '' || typeof documento.contratante.profissao == "undefined"){
        console.log('Aplicando Critica - C006-Profissao preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C006-Profissao preenchimento obrigatorio")
    }

    if(documento.contratante.enderecoResidencial === null || documento.contratante.enderecoResidencial === '' || typeof documento.contratante.enderecoResidencial == "undefined"){
        console.log('Aplicando Critica - C007-Endereço Residencial preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C007-Endereço Residencial preenchimento obrigatorio")
    }

    if(documento.contratante.email === null || documento.contratante.email === '' || typeof documento.contratante.email == "undefined"){
        console.log('Aplicando Critica - C008-Email preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C008-Email Residencial preenchimento obrigatorio")
    }

    if(documento.itemSegurado.tipo === null || documento.itemSegurado.tipo === '' || typeof documento.itemSegurado.tipo == "undefined"){
        console.log('Aplicando Critica - C009-Tipo do item Segurado preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C009-Tipo do item Segurado preenchimento obrigatorio")
    }else{

        switch (documento.itemSegurado.tipo) {
            case 'automovel':
              console.log('Tipo Item Segurado: automovel' + documento.itemSegurado.automovel);
              if(documento.itemSegurado.automovel === null || documento.itemSegurado.automovel === '' || typeof documento.itemSegurado.automovel == "undefined"){
                console.log('Aplicando Critica - C010-Tipo item segurado automovel preenchimento obrigatorio');
                documento.criticas.criticado=true
                criticas.push("C010-Tipo item segurado automovel preenchimento obrigatorio")
              }else{
                  if(documento.itemSegurado.automovel.modelo === null 
                    || documento.itemSegurado.automovel.modelo === '' 
                    || typeof documento.itemSegurado.automovel.modelo == "undefined"){
                        console.log('Aplicando Critica - C011-Modelo automovel preenchimento obrigatorio');
                        documento.criticas.criticado=true
                        criticas.push("C011-Modelo automovel preenchimento obrigatorio")
                    }

                    if(documento.itemSegurado.automovel.fabricante === null 
                        || documento.itemSegurado.automovel.fabricante === '' 
                        || typeof documento.itemSegurado.automovel.fabricante == "undefined"){
                            console.log('Aplicando Critica - C012-Fabricante automovel preenchimento obrigatorio');
                            documento.criticas.criticado=true
                            criticas.push("C012-Fabricante automovel preenchimento obrigatorio")
                    }

                    if(documento.itemSegurado.automovel.cor === null 
                        || documento.itemSegurado.automovel.cor === '' 
                        || typeof documento.itemSegurado.automovel.cor == "undefined"){
                            console.log('Aplicando Critica - C013-Cor automovel preenchimento obrigatorio');
                            documento.criticas.criticado=true
                            criticas.push("C013-Cor automovel preenchimento obrigatorio")
                    }

                    if(documento.itemSegurado.automovel.anoModelo === null 
                        || documento.itemSegurado.automovel.anoModelo === '' 
                        || typeof documento.itemSegurado.automovel.anoModelo == "undefined"){
                            console.log('Aplicando Critica - C014-Ano Modelo automovel preenchimento obrigatorio');
                            documento.criticas.criticado=true
                            criticas.push("C014-Ano Modelo automovel preenchimento obrigatorio")
                    }

                    if(documento.itemSegurado.automovel.anoFabricacao === null 
                        || documento.itemSegurado.automovel.anoFabricacao === '' 
                        || typeof documento.itemSegurado.automovel.anoFabricacao == "undefined"){
                            console.log('Aplicando Critica - C015-Ano Fabricacao automovel preenchimento obrigatorio');
                            documento.criticas.criticado=true
                            criticas.push("C015-Ano Fabricacao automovel preenchimento obrigatorio")
                    }
              }
            break;
            
            case 'vida':
              console.log('Tipo Item Segurado: vida');
              if(documento.itemSegurado.vida === null || documento.itemSegurado.vida === '' || typeof documento.itemSegurado.vida == "undefined"){
                console.log('Aplicando Critica - C016-Tipo item segurado vida preenchimento obrigatorio');
                documento.criticas.criticado=true
                criticas.push("C016-Tipo item segurado vida preenchimento obrigatorio")
            }else{
                if(documento.itemSegurado.vida.primeiroNome === null 
                  || documento.itemSegurado.vida.primeiroNome === '' 
                  || typeof documento.itemSegurado.vida.primeiroNome == "undefined"){
                      console.log('Aplicando Critica - C017-Primeiro nome vida preenchimento obrigatorio');
                      documento.criticas.criticado=true
                      criticas.push("C017-Primeiro nome vida preenchimento obrigatorio")
                  }

                  if(documento.itemSegurado.vida.sobrenome === null 
                      || documento.itemSegurado.vida.sobrenome === '' 
                      || typeof documento.itemSegurado.vida.sobrenome == "undefined"){
                          console.log('Aplicando Critica - C018-Sobrenome vida preenchimento obrigatorio');
                          documento.criticas.criticado=true
                          criticas.push("C018-Sobrenome vida preenchimento obrigatorio")
                  }

                  if(documento.itemSegurado.vida.dataDeNascimento === null 
                      || documento.itemSegurado.vida.dataDeNascimento === '' 
                      || typeof documento.itemSegurado.vida.dataDeNascimento == "undefined"){
                          console.log('Aplicando Critica - C019-Data de Nascimento vida preenchimento obrigatorio');
                          documento.criticas.criticado=true
                          criticas.push("C019-Data de Nascimento vida preenchimento obrigatorio")
                  }

                  if(documento.itemSegurado.vida.estadoCivil === null 
                      || documento.itemSegurado.vida.estadoCivil === '' 
                      || typeof documento.itemSegurado.vida.estadoCivil == "undefined"){
                          console.log('Aplicando Critica - C020-Estado Civil vida preenchimento obrigatorio');
                          documento.criticas.criticado=true
                          criticas.push("C020-Estado Civil vida preenchimento obrigatorio")
                  }

                  if(documento.itemSegurado.vida.cpf === null 
                      || documento.itemSegurado.vida.cpf === '' 
                      || typeof documento.itemSegurado.vida.cpf == "undefined"){
                          console.log('Aplicando Critica - C021-CPF vida preenchimento obrigatorio');
                          documento.criticas.criticado=true
                          criticas.push("C021-CPF vida preenchimento obrigatorio")
                  }

                  if(documento.itemSegurado.vida.profissao === null 
                    || documento.itemSegurado.vida.profissao === '' 
                    || typeof documento.itemSegurado.vida.profissao == "undefined"){
                        console.log('Aplicando Critica - C022-Profissao vida preenchimento obrigatorio');
                        documento.criticas.criticado=true
                        criticas.push("C022-Profissao vida preenchimento obrigatorio")
                }
            }
            break;    
            
            case 'residencia':
              console.log('Tipo Item Segurado: residencia');
              if(documento.itemSegurado.residencia === null || documento.itemSegurado.residencia === '' || typeof documento.itemSegurado.residencia == "undefined"){
                console.log('Aplicando Critica - C023-Tipo item segurado residencia preenchimento obrigatorio');
                documento.criticas.criticado=true
                criticas.push("C023-Tipo item segurado residencia preenchimento obrigatorio")
              }
            break;
            
            case 'aparelho':
              console.log('Tipo Item Segurado: aparelho');
              if(documento.itemSegurado.aparelho === null || documento.itemSegurado.aparelho === '' || typeof documento.itemSegurado.aparelho == "undefined"){
                console.log('Aplicando Critica - C024-Tipo item segurado aparelho preenchimento obrigatorio');
                documento.criticas.criticado=true
                criticas.push("C024-Tipo item segurado aparelho preenchimento obrigatorio")
              }
            break;  
            
            default:
              console.log('Tipo Item Segurado: invalido');
              documento.criticas.criticado=true
              criticas.push("C025-Tipo item segurado invalido")
          }

    }

    if(documento.cobertura.BAS <= 0 ||
        documento.cobertura.BAS === null || 
        documento.cobertura.BAS === '' || 
        typeof documento.cobertura.BAS == "undefined"){
        console.log('Aplicando Critica - C026-Cobertura BAS preenchimento obrigatorio');
        documento.criticas.criticado=true
        criticas.push("C026-Cobertura BAS preenchimento obrigatorio")
    }

    if(isNaN(documento.cobertura.BAS)){
        console.log('Aplicando Critica - C027-Cobertura BAS precisa ser numero');
        documento.criticas.criticado=true
        criticas.push("C027-Cobertura BAS precisa ser numero")
    }

    if(isNaN(documento.cobertura.FURTO) || 
             documento.cobertura.FURTO === null || 
             documento.cobertura.FURTO === '' || 
             typeof documento.cobertura.FURTO == "undefined"){
        console.log('Aplicando Critica - C028-Cobertura FURTO precisa ser numero');
        documento.criticas.criticado=true
        criticas.push("C028-Cobertura FURTO precisa ser numero")
    }

    if(isNaN(documento.cobertura.MORTE) || 
             documento.cobertura.MORTE === null ||
             documento.cobertura.MORTE === '' ||
             typeof documento.cobertura.MORTE == "undefined"){
        console.log('Aplicando Critica - C029-Cobertura MORTE precisa ser numero');
        documento.criticas.criticado=true
        criticas.push("C029-Cobertura MORTE precisa ser numero")
    }    
      
    //Atribui as criticas ao documento original
    documento.criticas.listaDeCriticas=criticas

    //retornod da função
    return documento
};

//Função responsável por validar os documentos de entrada
exports.validarDocumentos = function(documento) {
    console.log('Validar Documentos');

    // Variaveis de criticas
    var criticas

    //Valida se ja não tem criticas no documento, se não tiver cria a lista do zero, se tiver apenda ao documento existe
    if(documento.criticas.listaDeCriticas === null){
        criticas = []
    }else{
        criticas = documento.criticas.listaDeCriticas
    }
    
    //declara variaveis responsáveis por calcular o CPF
    var Soma;
    var Resto;
    Soma = 0;
    var strCPF = documento.contratante.cpf

    //Se CPF preenchido com Zero, aplicar regra
    if (strCPF == "00000000000"){
        console.log('Aplicando Critica - C030-CPF Invalido');
        documento.criticas.criticado=true
        criticas.push("C030-Cobertura BAS preenchimento obrigatorio")
    }

    //Bloco de regras de calculo de CPF
    for (let i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ){
        console.log('Aplicando Critica - C031-CPF Invalido');
        documento.criticas.criticado=true
        criticas.push("C031-CPF Invalido")
    }

    Soma = 0;
    for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ){
        console.log('Aplicando Critica - C032-CPF Invalido');
        documento.criticas.criticado=true
        criticas.push("C032-CPF Invalido")
    }

    //Adicona as criticas ao documento JSON completo
    documento.criticas.listaDeCriticas=criticas

    //Retorno da função
    return documento
};