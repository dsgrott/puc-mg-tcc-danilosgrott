/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: statusController.js
 * Objetivo: Modulo responsável por abrir uma página simples com status do servidor
 */

'use strict';

//Imports de modulos obrigatórios
var path = require( 'path' );

//Função responsável por apresentar o status em tela
exports.apresentarStatus = function(req, res) {
    //Chama html 
    res.sendFile(path.join(path.resolve('src/view/status.html')));
};




