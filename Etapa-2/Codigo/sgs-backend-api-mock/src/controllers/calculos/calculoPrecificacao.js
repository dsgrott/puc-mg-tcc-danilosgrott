/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: CalculoPrecificacao.sj
 * Objetivo: Responsável por aplicar o preço do premio no seguro
 */

'use strict';

//Função responsável por aplicar o preço no seguro
exports.aplicarPreco = function(documento) {
    console.log('Aplicando Preco');

    //Variavel responsável por armazenar o percentual de calculo
    var percentualDeCalculo = 0

    //Switch para definir o percentual do de calculo de acordo com o produto
    switch (documento.itemSegurado.tipo) {
        case 'automovel':
            percentualDeCalculo = 0.06
            break;
        case 'vida':
            percentualDeCalculo = 0.01
            break;
        case 'residencia':
            percentualDeCalculo = 0.04
            break;
        case 'aparelho':
            percentualDeCalculo = 0.02
            break;                        
        default:
            percentualDeCalculo = 0
    }

    //Variveis com o valor das coberturas
    var BAS = parseFloat(documento.cobertura.BAS)
    var FURTO = parseFloat(documento.cobertura.FURTO)
    var MORTE = parseFloat(documento.cobertura.MORTE)
    
    //Calculo do premio mock
    var premio = ((BAS + FURTO + MORTE) * percentualDeCalculo)

    //Criação do layout JSON de opçoes de compra para se utilizado em outro metodo
    var opcoesDeCompra = {opcoesDeCompra: {valorTotal:premio.toFixed(2),
        formaPagamento:[
                {
                    id:null,
                    forma:null,
                    tipo:null,
                    parcelas:null,
                    valor:null
                }
            ]
        }   
     }

    //Junção dos dois json em um unico json
    documento = Object.assign(documento, opcoesDeCompra )

    //retorno do documento
    return documento
};