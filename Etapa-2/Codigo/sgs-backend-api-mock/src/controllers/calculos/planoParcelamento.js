/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: planoParcelamento.js
 * Objetivo: Modulo responsável por montar a rota de parcelamento que sera gerenciada pelo legado (Financeiro e Contabilidade)
 */

'use strict';

//Função responsável por aplicar as parcelas do JSON
exports.aplicarParcelas= function(documento) {
    console.log('Aplicando Forma Pagamento Disponivel');

     //Array de parcelas
     var parcelas = []
     
     //Variavel para controlar as data de vencimento
     var data = new Date(); 

     //Lopping que percorre o plano solicitado pelo cliente para montar a quantidade de parcelas
     for(var i = 1; i <= documento.opcoesDeSelecao.formaPagamento.parcelas;i++){

        //layout parcela do JSON
         var parcela = {
            parcela:0,
            valor:0,
            status:"",
            vencimento:""
         }

         //Recupera o valor da forma de pagamento selecionada pelo cliente
         var valor = parseFloat(documento.opcoesDeSelecao.formaPagamento.valor).toFixed(2)

         //Atribui o numero da parcela de acordo com o looping e seleção do cliente
         parcela.parcela=i + ""

         //Atribui o valor da parcela
         parcela.valor =  valor

         //Atribui o status Atual que futuramente é manipulado pelo time do legado
         parcela.status = "Pendente"

         //Seta 30 dias para o vencimento de cada parcela
         if(i != 1){
            data.setDate(data.getDate() + 30);
         }

         let dia  = data.getDate().toString()
         let diaF = (dia.length == 1) ? '0'+ dia : dia
         let mes  = (data.getMonth()+1).toString()
         let mesF = (mes.length == 1) ? '0'+ mes : mes
         let anoF = data.getFullYear()
        
         parcela.vencimento = anoF + "-" + mesF + "-" + diaF

         //Adciona Parcela ao array de parcelas
         parcelas.push(parcela)

     }

     //Adciona o json de parcelas ao documento de JSON completo
     documento.parcelas = parcelas

    //Retorno da função
    return documento
};