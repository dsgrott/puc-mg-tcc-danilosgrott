/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: formaPagamento.js
 * Objetivo: Modulo responsável devolver o plano de forma de pagamento disponivel na contratação
 */


'use strict';

//Função responsável por aplicar as formas de pagamento disponivel
exports.aplicarFormaPagamento = function(documento) {
    console.log('Aplicando Forma Pagamento Disponivel');

    //Variavel para amarzenar o valor do premio Calculado
    var valorTotal = parseFloat(documento.opcoesDeCompra.valorTotal)

    //layout do objeto json forma de pagamento com as formas de pagamento disponivel Mock
    var formaPagamento = [
                {
                    id:"B1",
                    forma:"A vista no boleto de R$ " + valorTotal.toFixed(2),
                    tipo:"Boleto",
                    parcelas:1,
                    valor:valorTotal.toFixed(2)
                },
                {
                    id:"B2",
                    forma:"2x sem juros no boleto de R$ " + (valorTotal / 2).toFixed(2),
                    tipo:"Boleto",
                    parcelas:2,
                    valor:(valorTotal / 2).toFixed(2)
                },
                {
                    id:"B3",
                    forma:"3x sem juros no boleto de R$ " + (valorTotal / 3).toFixed(2),
                    tipo:"Boleto",
                    parcelas:3,
                    valor:(valorTotal / 3).toFixed(2)
                },
                {
                    id:"B4",
                    forma:"4x sem juros no boleto de R$ " + (valorTotal / 4).toFixed(2),
                    tipo:"Boleto",
                    parcelas:4,
                    valor:(valorTotal / 4).toFixed(2)
                },
                {
                    id:"B5",
                    forma:"5x sem juros no boleto de R$ " + (valorTotal / 5).toFixed(2),
                    tipo:"Boleto",
                    parcelas:5,
                    valor:(valorTotal / 5).toFixed(2)
                },
                {
                    id:"B6",
                    forma:"6x sem juros no boleto de R$ " + (valorTotal / 6).toFixed(2),
                    tipo:"Boleto",
                    parcelas:6,
                    valor:(valorTotal / 6).toFixed(2)
                },
                {
                    id:"B7",
                    forma:"7x sem juros no boleto de R$ " + (valorTotal / 7).toFixed(2), 
                    tipo:"Boleto",
                    parcelas:7,
                    valor:(valorTotal / 7).toFixed(2)
                },
                {
                    id:"B8",
                    forma:"8x sem juros no boleto de R$ " + (valorTotal / 8).toFixed(2),
                    tipo:"Boleto",
                    parcelas:8,
                    valor:(valorTotal / 8).toFixed(2)
                },
                {
                    id:"B9",
                    forma:"9x sem juros no boleto de R$ " + (valorTotal / 9).toFixed(2),
                    tipo:"Boleto",
                    parcelas:9,
                    valor:(valorTotal / 9).toFixed(2)
                },
                {
                    id:"B10",
                    forma:"10x com juros no boleto de R$ " + ((valorTotal / 10) + (valorTotal * 0.02)).toFixed(2),
                    tipo:"Boleto",
                    parcelas:10,
                    valor:((valorTotal / 10) + (valorTotal * 0.02)).toFixed(2)
                },
                {
                    id:"B11",
                    forma:"11x com juros no boleto de R$ " + ((valorTotal /11) + (valorTotal * 0.02)).toFixed(2),
                    tipo:"Boleto",
                    parcelas:11,
                    valor:((valorTotal /11) + (valorTotal * 0.02)).toFixed(2)
                },
                {
                    id:"B12",
                    forma:"12x com juros no boleto de R$ " + ((valorTotal/ 12) + (valorTotal * 0.02)).toFixed(2),
                    tipo:"Boleto",
                    parcelas:12,
                    valor:((valorTotal/ 12) + (valorTotal * 0.02)).toFixed(2)
                }
            ] 

    //Atribuindo o objeto forma de pagamento ao JSON de retorno
    documento.opcoesDeCompra.formaPagamento =  formaPagamento

    //retorno da função
    return documento
};