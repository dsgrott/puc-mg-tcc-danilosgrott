/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: dadosDocumentoController.js
 * Objetivo: Modulo responsável por controlar o fluxo de consulta e gravação de dados
 */

'use strict';

//Import de modulos e variaveis importantes
const { Console } = require('console');
var path      = require( 'path' );
var security  = require( path.resolve( 'src/controllers/securityController' ) );
var mongoose = require('mongoose'),
Doc = mongoose.model('SGSDocumentos');

//Função responsável por consultar registros no banco
exports.list_doc = function(req, res) {

    //Valida autorização de acesso MOCK
    security.autorizacao(req,res);

    //Se docId Nulo busca a base toda, senão somente um ID especifico
    if(req.query.docId == null){
      Doc.find({}, function(err, doc) {
        if (err)
          res.send(err);
        res.json(doc);
      }).sort({"Created_date": -1}).limit(30);
    }else{
      Doc.findById(req.query.docId, function(err, doc) {
        if (err)
          res.send(err);
        res.json(doc);
      });
    }

  };
  
  //Função responsável por criar um registros no banco
  exports.create_a_doc = function(req, res) {

    //Valida autorização de acesso MOCK
    security.autorizacao(req,res)
    
    //Recuper o json e mapeia como um schema do banco
    var new_doc = new Doc(req.body);

    //Modifica o status do documento ao salvar
    if(new_doc.criticas.criticado){
      new_doc.status = 'Criticado'
    }else{
      new_doc.status = 'Validado'
    }

    //Salva documento
    new_doc.save(function(err, doc) {
      if (err)
        res.send(err);
      res.json(doc);
    });
  };
  
  //Função responsável por atualizar um registros no banco
  exports.update_a_doc = function(req, res) {

    //Valida autorização de acesso MOCK
    security.autorizacao(req,res)

    console.log("Resultado query: " + req.query.docId)

    //Atualiza o registro
    Doc.findOneAndUpdate({ _id: req.query.docId }, req.body, {new: true}, function(err, doc) {
      if (err)
        res.send(err);
      res.json(doc);
    });

  };
  
  //Função responsável por deletar um registros no banco
  exports.delete_a_doc = function(req, res) {
  
    //Valida autorização de acesso MOCK
    security.autorizacao(req,res)

    //Remove registro
    Doc.remove({_id: req.query.docId}, function(err, doc) {
      if (err)
        res.send(err);
      res.json(doc);
    });
  };
