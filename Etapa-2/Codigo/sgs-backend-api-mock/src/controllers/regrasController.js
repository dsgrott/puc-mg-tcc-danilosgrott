/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: regrasController.js
 * Objetivo: Modulo responsável por controlar o fluxo de regras da aplicação
 */

'use strict';

//Imports de modulos obrigatórios
var path = require( 'path' );
var security        = require( path.resolve( 'src/controllers/securityController' ) );
var regrasDeCampo   = require( path.resolve( 'src/controllers/regras/regrasDeCampo' ) );
var regrasDeNegocio = require( path.resolve( 'src/controllers/regras/regrasDeNegocio' ) );

//Função responsável por controlar o fluxo dos processamentos de regras
exports.processar_regras = function(req, res) {
    console.log('Processar regras ');

    //Valida segurança MOCK
    security.autorizacao(req,res)

    //Variavel com o JSON de entrada
    var documento = req.body
    console.log(req)
    console.log(req.body)

    //console.log("Doc: " + documento + "req: " + Object.values(req.body))
    //console.log("req2: " + Object.values(req))

    //Chama as funçoes de regras de campo e de negocios
    documento.criticas.criticado=false
    documento.criticas.listaDeCriticas=null
    documento = regrasDeCampo.validarNullCamposObrigatorios(documento)
    documento = regrasDeCampo.validarDocumentos(documento)
    documento = regrasDeNegocio.limiteIdade(documento)
    documento = regrasDeNegocio.rangeAtendimentoCEP(documento)
    documento = regrasDeNegocio.validarLimites(documento)
    
    //Força alteração de status
    if(documento.criticas.criticado){
        documento.status = "Criticado"
    }else{
        documento.status = "Validado"
    }

    console.log(req.body)

    //Retorno do Json
    res.json(documento);
};

