/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: securityController.js
 * Objetivo: Modulo responsável por controlar o fluxo autenticação e autorização MOCK
 */

'use strict';

//Função responsável por controlar o fluxo de autenticação e autorização MOCK
exports.autorizacao = function(req, res) {
    
    //Recupera o header x-access-token com token mock
    const token = req.headers['x-access-token'];

    //se header não existir retornar 401
    if (!token){
      res.status(401).json({ auth: false, message: 'Nenhum token fornecido' });
      next()
    }

    //se header não for valido retornar 401
    if (token != process.env.X_ACCESS_TOKEN){
      res.status(401).json({ auth: false, message: 'Token Invalido' });
      next()
    }
};




