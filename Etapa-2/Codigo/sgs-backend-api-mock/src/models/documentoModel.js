/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: documentoModel.js
 * Objetivo: Modulo responsável por montar o modelo do documento no banco de dados
 */

'use strict';

//Imports de modulos obrigatórios
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Variavel com o modelo de schema para o documento seguro
var DocumentoSchema = new Schema({
  contratante: {
    type: Object,
    required: 'Entre com os dados do contratante'
  },
  itemSegurado: {
    type: Object,
    required: 'Entre com o item segurado'
  },
  cobertura: {
    type: Object,
    required: 'Informe as coberturas'
  },
  criticas: {
    type: Object,
    required: 'Criticas precisa ser imformado mesmo atribudos nulos'
  },  
  opcoesDeCompra: {
    type: Object,
  },
  opcoesDeSelecao: {
    type: Object,
  },
  parcelas: {
    type: Object,
  },
  integracoes: {
    type: Array,
  }, 
  Created_date: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['Novo','Validado', 'Criticado', 'Calculado', 'Emitido']
    }],
    default: ['Novo']
  }
});

//Adiciona o documento ao modelo mongoDB
module.exports = mongoose.model('SGSDocumentos', DocumentoSchema);