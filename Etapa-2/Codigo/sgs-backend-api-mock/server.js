/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: serve.js
 * Objetivo: Modulo inicial da aplicação (Main) possui as configurações inicias do server
 */

//Import de modulos e variaveis importantes
var path = require( 'path' );
const { nextTick } = require('process');
var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
mongoose = require('mongoose'),
Documento   = require(path.resolve( './src/models/documentoModel')), //created model loading here
bodyParser = require('body-parser');

// mongoose URL de conexão de instância
mongoose.Promise = global.Promise;
mongoose.connect(process.env.URL_DATABASE); 

//Utulizado modelo JSON
app.use(express.json());

//Importando rotas
var routes = require( path.resolve( 'routes' )); 

//registro de rotas
routes(app); 

//Iniciando o listener de porta
app.listen(port);

console.log('API de Regras, Calculo, Consulta e Gravação de Dados Mock no ar: ' + port);

//Valida se foi chamado alguma rota inexistente e retorna 404
app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});