
/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: Camada de Interface de Usuário - Componente Web Responsivo
 * Autor: Danilo Sgrott
 * Modulo: etapasForm.js
 * Objetivo: JavaScript de controle da pagina de emissao
 */

//função responsável por habilitar o preenchimento do item vida ou automovel.
function itemSeguradoTipoSelecionado(value){

    //console.log("Valor: " + value )
    var tipoAutomovel = document.getElementsByClassName('itemAutomovel');
    var itemVida = document.getElementsByClassName('itemVida');
    var itemResidencia = document.getElementsByClassName('itemResidencia');
    var itemAparelho = document.getElementsByClassName('itemAparelho');
    var itemCobertura = document.getElementsByClassName('itemCobertura');

    if(value == "automovel"){
        tipoAutomovel[0].style.display = 'block';
        itemVida[0].style.display = 'none';
        itemResidencia[0].style.display = 'none';
        itemAparelho[0].style.display = 'none';
        itemCobertura[0].style.display = 'block';

        document.getElementById("fabricante").setAttribute("required", "true");
        document.getElementById("modelo").setAttribute("required", "true");
        document.getElementById("cor").setAttribute("required", "true"); 
        document.getElementById("anoModelo").setAttribute("required", "true");             
        document.getElementById("anoFabricacao").setAttribute("required", "true");  




    }else if(value == "vida"){
        tipoAutomovel[0].style.display = 'none';
        itemVida[0].style.display = 'block';
        itemResidencia[0].style.display = 'none';
        itemAparelho[0].style.display = 'none';
        itemCobertura[0].style.display = 'block';

        document.getElementById("itemPrimeiroNome").setAttribute("required", "true"); 
        document.getElementById("itemSobrenome").setAttribute("required", "true");  
        document.getElementById("itemDataDeNascimento").setAttribute("required", "true");  
        document.getElementById("itemEstadoCivil").setAttribute("required", "true");  
        document.getElementById("cpfItem").setAttribute("required", "true");  
        document.getElementById("profissaoItem").setAttribute("required", "true");  
        document.getElementById("itemPrimeiroNome").setAttribute("required", "true");  

        document.getElementById("fabricante").removeAttribute("required");
        document.getElementById("modelo").removeAttribute("required");
        document.getElementById("cor").removeAttribute("required");
        document.getElementById("anoModelo").removeAttribute("required");             
        document.getElementById("anoFabricacao").removeAttribute("required");



    }else if(value == "residencia"){
        tipoAutomovel[0].style.display = 'none';
        itemVida[0].style.display = 'none';
        itemResidencia[0].style.display = 'block';
        itemAparelho[0].style.display = 'none';
        itemCobertura[0].style.display = 'none';

    }else if(value == "aparelho"){
        tipoAutomovel[0].style.display = 'none';
        itemVida[0].style.display = 'none';
        itemResidencia[0].style.display = 'none';
        itemAparelho[0].style.display = 'block';
        itemCobertura[0].style.display = 'none';

    }else{
        tipoAutomovel[0].style.display = 'none';
        itemVida[0].style.display = 'none';
        itemResidencia[0].style.display = 'none';
        itemAparelho[0].style.display = 'none';
        itemCobertura[0].style.display = 'none';
    }
    
    
    
}

//tratamento e mascara dos campos
function mascara(i,t){
   
    var v = i.value;
    
    if(isNaN(v[v.length-1])){
       i.value = v.substring(0, v.length-1);
       return;
    }
    
    if(t == "data"){
       i.setAttribute("maxlength", "10");
       if (v.length == 2 || v.length == 5) i.value += "/";
    }
 
    if(t == "cpf"){
       i.setAttribute("maxlength", "14");
       if (v.length == 3 || v.length == 7) i.value += ".";
       if (v.length == 11) i.value += "-";
    }
 
    if(t == "cnpj"){
       i.setAttribute("maxlength", "18");
       if (v.length == 2 || v.length == 6) i.value += ".";
       if (v.length == 10) i.value += "/";
       if (v.length == 15) i.value += "-";
    }
 
    if(t == "cep"){
       i.setAttribute("maxlength", "9");
       if (v.length == 5) i.value += "-";
    }
 
    if(t == "tel"){
       
       let valorAtual = i.value

       if(i.value.length == 1){
       i.value = "("
       i.value += valorAtual
       }

       if(i.value.length == 3){
         i.value += ")"
      }

      if(i.value.length >= 5){

         if(v[4] == 9){
            i.setAttribute("maxlength", "14");
            if (v.length == 9) i.value += "-";
         }else{
            i.setAttribute("maxlength", "13");
            if (v.length == 8) i.value += "-";
         }
      
      }

      
    }
 }


$(document).ready(function() {
   $('.btn-theme').click(function(){
       $('#aguarde, #blanket').css('display','block');
   });
});

