/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: Documento.js
 * Objetivo: Modulo responsável criar o objeto padrão de documento
 */

'use strict';

//Modelo do objeto Documento
module.exports = class Documento {
    constructor(contratante, itemSegurado, 
        cobertura, criticas, opcoesDeCompra, 
        opcoesDeSelecao, parcelas,integracoes, status, _id, Created_date) {
        
            this.contratante = contratante
            this.itemSegurado = itemSegurado
            this.cobertura = cobertura
            this.criticas = criticas
            this.opcoesDeCompra = opcoesDeCompra
            this.opcoesDeSelecao = opcoesDeSelecao
            this.parcelas = parcelas
            this.integracoes = integracoes
            this.status = status
            this._id = _id
            this.Created_date = Created_date
    }
};