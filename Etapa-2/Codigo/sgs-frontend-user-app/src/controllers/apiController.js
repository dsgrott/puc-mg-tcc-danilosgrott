/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: Camada de Interface de Usuário - Componente Web Responsivo
 * Autor: Danilo Sgrott
 * Modulo: apiController.js
 * Objetivo: Modulo responsavel por controlar todos os fluxos a API externas e internas
 */

'use strict';

//Imports de modulos obrigatórios
const axios = require('axios');

//Preenchimento do header
var postheaders = { headers:{
   'Content-Type' : 'application/json',
   'x-api-key': process.env.X_API_KEY
   }
};

//Variaveis uteis
var URL_API =  process.env.URL_API
var PATH_REGRAS =  process.env.PATH_REGRAS
var PATH_DADOS =  process.env.PATH_DADOS
var PATH_CALCULOS = process.env.PATH_CALCULOS

//Função responsavel por chamar a api de regras e validar as regras
exports.regrasValidar = async function(docAPI, user) {

   //adicionando o registro da integração no documento
   docAPI.integracoes.push("API Regras - " + user + " - " + new Date().toISOString().slice(0, 10))

   //acionando a api externa de regras 
   const res = await axios.post(URL_API + PATH_REGRAS, docAPI, postheaders )
   
   //retornando o retorno da api
   return res.data

};

//Função responsavel por chamar a api de dados e incluir um registro
exports.incluirNovoRegistro = async function(docAPI, user) {

   //variavei de response
   let res

   console.log("docAPI._id: " + docAPI._id)
   console.log(docAPI)

   //adicionando o registro da integração no documento
   docAPI.integracoes.push("API Dados Incluir - " + user + " - "+ new Date().toISOString().slice(0, 10))

   //validando se é um insert ou update e chamada a api externa de dados
   if(docAPI._id == '' || docAPI._id === null || typeof docAPI._id == "undefined"){
      console.log("insere registro")
      res = await axios.post(URL_API + PATH_DADOS, docAPI, postheaders )
   }else{
      console.log("atualiza registro")
      res = await axios.put(URL_API + PATH_DADOS + "?docId=" + docAPI._id, docAPI, postheaders)
   }

   //retorno da api
   return res.data

};

//Função responsavel por chamar a api de dados e consultar os 30 ultimos registros
exports.buscarTudo = async function() {

   //chama api de dados para consulta
   let res = await axios.get(URL_API + PATH_DADOS, postheaders )

   //retorno dos dados
   return res.data

};

//Função responsavel por chamar a api de dados e consultar por id
exports.buscarPorID = async function(id, user) {

   console.log("docAPI._id: " + id)

   //chamada a api de dados para consulta por id
   let res = await axios.get(URL_API + PATH_DADOS  + "?docId=" + id,  postheaders )

   //retorno dos dados
   return res.data

};

//Função responsavel por chamar a api de calculos e calcular o documento
exports.calcular = async function(docAPI, user) {

    //adicionando o registro da integração no documento
   docAPI.integracoes.push("API Calcular - " + user + " - "+ new Date().toISOString().slice(0, 10))

   //chamada a api de calculos
   const res = await axios.post(URL_API + PATH_CALCULOS, docAPI, postheaders )
   
   //retorno da api
   return res.data

};

//Função responsavel por chamar a api de calculos para emitir o documento
exports.emitir = async function(docAPI, user) {

   //adicionando o registro da integração no documento
   docAPI.integracoes.push("API Emitir - " + user + " - "+ new Date().toISOString().slice(0, 10))

   //chamada a api de calculos
   const res = await axios.put(URL_API + PATH_CALCULOS + "?docId=" + docAPI._id, docAPI, postheaders )
   
   //retorno da api
   return res.data

};

//Função responsavel mockar e simular o envio dos dados a contabilidade
exports.enviarParaContabilidadeMock = function(docAPI, user) {

   //adicionando o registro da integração no documento
   docAPI.integracoes.push("Legado Contabilidade - " + user + " - "+ new Date().toISOString().slice(0, 10))

   //retorno da simulação
   return docAPI

};

//Função responsavel mockar e simular o envio dos dados ao financeiro
exports.enviarParaFinanceiro = function(docAPI, user) {

   //adicionando o registro da integração no documento
   docAPI.integracoes.push("Legado Financeiro - " + user + " - "+ new Date().toISOString().slice(0, 10))

    //retorno da simulação
    return docAPI

};

