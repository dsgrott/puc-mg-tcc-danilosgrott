/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: Camada de Interface de Usuário - Componente Web Responsivo
 * Autor: Danilo Sgrott
 * Modulo: pesquisaController.js
 * Objetivo: Modulo responsavel por controlar todos os fluxos de consulta e pequisa
 */

'use strict';

//Imports de modulos obrigatórios
const path = require( 'path' );
const security    = require(path.resolve( 'src/controllers/securityMockController' ));
const api    = require(path.resolve( 'src/controllers/apiController' ));

//Função responsável por carregar a pagina de pesquisa
exports.carregarPaginaDePesquisa =  async function(req, res) {

    //validando segurança
    var logado = security.validarAcesso(req,res)

    //acessando a api de dados
    let retorno = await api.buscarTudo()
    
    //criando a variavel de retorno da pagina
    let docs = {documentos:[]}

    //formatando documentos para aparecer em tela
    for (let value of retorno) {
        docs.documentos.push(value)
    }

    //console.log(JSON.stringify(docs))

    //se estiver logado direcionar para pagina de consulta
    if(logado){
        res.render('consultaPage', docs )
    }else{
        res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
    }

};

//Função responsável por capturas a proposta selecionada pelo usuário e direcionar a pagina de emissão
exports.redirecionarProposta =  async function(req, res) {

    //validando acesso
    var logado = security.validarAcesso(req,res)

    console.log("metodo Novo")
    console.log(req.body.postId)

    //acessando api de dados
    let retorno = await api.buscarPorID(req.body.postId, req.session.userid)

    //console.log(JSON.stringify(retorno))
    
    //se estiver logado direcionar para a página de emissão
    if(logado){
        res.render('emissaoPage', retorno )
    }else{
        res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
    }

};



