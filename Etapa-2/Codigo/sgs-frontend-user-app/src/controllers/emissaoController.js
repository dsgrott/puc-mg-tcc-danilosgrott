/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: Camada de Interface de Usuário - Componente Web Responsivo
 * Autor: Danilo Sgrott
 * Modulo: emissaoController.js
 * Objetivo: Modulo responsavel por controlar todos os fluxos de emissao da aplicação
 */

'use strict';

//Imports de modulos obrigatórios
const path = require( 'path' );
const security    = require(path.resolve( 'src/controllers/securityMockController' ));
const api    = require(path.resolve( 'src/controllers/apiController' ));
const Documento    = require(path.resolve( 'src/models/Documento' ));

//Função responsável por carregar a tela inicial de emissao
exports.carregarPaginaDeEmissao =  function(req, res) {

    //validando segurança
    var logado = security.validarAcesso(req,res)

    console.log(logado)

    //variaveis importantes para o fluxo da aplicação
    let contratante = {
        primeiroNome:null,
        sobrenome:null,
        dataDeNascimento:null,
        estadoCivil:null,
        cpf:null,
        telefone:null,
        profissao:null,
        enderecoResidencial:null,
        bairroResidencial:null,
        cidadeResidencial:null,
        cepResidencial:null,
        email:null
     }
 
 let itemSegurado = {tipo:null,
                     automovel:{
                         modelo:null,
                         fabricante:null,
                         cor:null,
                         anoModelo:null,
                         anoFabricacao:null
                     },
                     vida:{
                         primeiroNome:null,
                         sobrenome:null,
                         dataDeNascimento:null,
                         estadoCivil:null,
                         cpf:null,
                         profissao:null
                    },
                    residencia:{},
                    aparelho:{}}
 
 let cobertura = {
     BAS:null,
     FURTO:null,
     MORTE:null
 }
 
 let criticas = {
         criticado:false,
         listaDeCriticas:[]
     }
 
 
 let opcoesDeCompra = {
         valorTotal:null,
         formaPagamento:[
           {
             forma:null,
             tipo:null,
             parcelas:null,
             valor:null
           }
        ]
     }
 
 let opcoesDeSelecao = {
         formaPagamento:{
             id:null,
             forma:null,
             tipo:null,
             parcelas:null,
             valor:null
        },
        dadosBancarios:null,
        cartaoCredito:null
     }
 
    let parcelas= [{
         parcela:null,
         valor:null,
         status:null,
         vencimento:"0000-00-00"
     }]
 
    let integracoes = []     
    let status = "Novo"
    let _id
    let Created_date

    let documento = new Documento(contratante, 
                                    itemSegurado,
                                    cobertura,
                                    criticas,
                                    opcoesDeCompra,
                                    opcoesDeSelecao,
                                    parcelas,
                                    integracoes,
                                    status,
                                    _id,
                                    Created_date);

    console.log("documento criado")
    console.log(documento)

    console.log("requi")
    console.log(req.body)

    //caso esteja loga direciona para a tela de emissao, caso contrario pede login
    if(logado){
        res.render('emissaoPage', documento)
    }else{
        res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
    }

};

//Função responsável por validar e calcular a proposta
exports.validarProposta = async function(req, res) {

    //validando segurança
    var logado = security.validarAcesso(req,res)

    //variaveis importantes para o fluxo da aplicação
    let contratante = {
        primeiroNome:null,
        sobrenome:null,
        dataDeNascimento:null,
        estadoCivil:null,
        cpf:null,
        telefone:null,
        profissao:null,
        enderecoResidencial:null,
        bairroResidencial:null,
        cidadeResidencial:null,
        cepResidencial:null,
        email:null
     }
 
    let itemSegurado = {tipo:null,
                        automovel:{
                            modelo:null,
                            fabricante:null,
                            cor:null,
                            anoModelo:null,
                            anoFabricacao:null
                        },
                        vida:{
                            primeiroNome:null,
                            sobrenome:null,
                            dataDeNascimento:null,
                            estadoCivil:null,
                            cpf:null,
                            profissao:null
                        },
                        residencia:{},
                        aparelho:{}}
 
        let cobertura = {
            BAS:null,
            FURTO:null,
            MORTE:null
        }
    
    let criticas = {
            criticado:false,
            listaDeCriticas:[null]
        }
 
 
    let opcoesDeCompra = {
            valorTotal:null,
            formaPagamento:[
            {
                id:null,
                forma:null,
                tipo:null,
                parcelas:null,
                valor:null
            }
            ]
        }
 
    let opcoesDeSelecao = {
            formaPagamento:{
                forma:null,
                tipo:null,
                parcelas:null,
                valor:null
            },
            dadosBancarios:null,
            cartaoCredito:null
        }
    
    let parcelas= [{
            parcela:null,
            valor:null,
            status:null,
            vencimento:"0000-00-00"
        }]
    
    let integracoes = []     
    let status = null
    let _id
    let Created_date

    let documento = new Documento(contratante, 
        itemSegurado,
        cobertura,
        criticas,
        opcoesDeCompra,
        opcoesDeSelecao,
        parcelas,
        integracoes,
        status,
        _id,
        Created_date);

    //console.log("Gerou no documento iterno")
    //console.log(documento)

    //console.log("Veio na Requisicao")
    //console.log(req.body)

    //popula objeto com os dados preenchidos em tela
    documento.contratante.primeiroNome = req.body.primeiroNome
    documento.contratante.sobrenome = req.body.sobrenome
    documento.contratante.dataDeNascimento = req.body.nascimento
    documento.contratante.estadoCivil = req.body.estadoCivil
    documento.contratante.cpf = req.body.cpf
    documento.contratante.telefone = req.body.telefone
    documento.contratante.profissao = req.body.profissao
    documento.contratante.enderecoResidencial = req.body.endereco
    documento.contratante.bairroResidencial = req.body.endereco2
    documento.contratante.cidadeResidencial = req.body.estado
    documento.contratante.cepResidencial = req.body.cep
    documento.contratante.email = req.body.email

    documento.itemSegurado.tipo = req.body.itemSeguradoTipo

    if (documento.itemSegurado.tipo == "automovel"){
        documento.itemSegurado.automovel.modelo =  req.body.modelo
        documento.itemSegurado.automovel.fabricante = req.body.fabricante
        documento.itemSegurado.automovel.cor = req.body.cor
        documento.itemSegurado.automovel.anoModelo = req.body.anoModelo
        documento.itemSegurado.automovel.anoFabricacao = req.body.anoFabricacao
    }

    if (documento.itemSegurado.tipo == "vida"){
        documento.itemSegurado.vida.primeiroNome =  req.body.itemPrimeiroNome
        documento.itemSegurado.vida.sobrenome = req.body.itemSobrenome
        documento.itemSegurado.vida.dataDeNascimento = req.body.itemDataDeNascimento
        documento.itemSegurado.vida.estadoCivil = req.body.itemEstadoCivil
        documento.itemSegurado.vida.cpf = req.body.cpfItem
        documento.itemSegurado.vida.profissao = req.body.profissaoItem
    }

    if (documento.itemSegurado.tipo == "residencia"){
        documento.itemSegurado.residencia = {}
    }

    if (documento.itemSegurado.tipo == "aparelho"){
        documento.itemSegurado.aparelho = {}
    }

    documento.cobertura.BAS = req.body.BAS
    documento.cobertura.FURTO = req.body.FURTO
    documento.cobertura.MORTE = req.body.MORTE

    documento.criticas.listaDeCriticas = [""]
    

    documento.status = req.body.status

    //atribuição de ID caso exista
    if (req.body.id != ''){
        documento._id = req.body.id

        let documentoDB = await api.buscarPorID(documento._id, req.session.userid)
        documento.integracoes = documentoDB.integracoes
    }else{
        documento.integracoes = []
    }

    //validação de campo de data de criação caso exista
    if (req.body.createDate != ''){
        documento.Created_date = req.body.createDate
    }

    console.log("Antes de regrasValidar")
    //console.log(documento)

    //chamando controller de regras
    documento =  await api.regrasValidar(documento, req.session.userid)

    console.log("Depois de regrasValidar")
    //console.log(documento)

    //chamando controller de dados
    documento =  await api.incluirNovoRegistro(documento, req.session.userid)

    console.log("Depois de incluirNovoRegistro")
    //console.log(documento)

    //caso o documento volte criticado devolver a tela para o usuário, caso contrario seguir com o calculo
    if(documento.status == "Criticado"){

        console.log("Foi criticado")

        //se estiver logado, direcionar a pagina para o usuário
        if(logado){
            res.render('emissaoPage', documento)
        }else{
           res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
        }
        
    }else{

        console.log("Passou da Critica")

        console.log("Antes do calcular ")

        //chamando controller de calculos
        documento =  await api.calcular(documento, req.session.userid)

        console.log("Antes do incluirNovoRegistro ")
        //console.log(documento)
    
        //chamando controller de dados
        documento =  await api.incluirNovoRegistro(documento, req.session.userid)
    
        console.log("Depois do incluirNovoRegistro")
        //console.log(documento)
    
        //se estiver logado, direcionar a pagina para o usuário
        if(logado){
            if(documento.status == "Calculado") {res.render('emissaoPage', documento)}
        }else{
            res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
        }

    }

};

//Função responsável por emitir proposta
exports.emitirProposta = async function(req, res) {

    console.log(req.body)

    //validando segurança
    var logado = security.validarAcesso(req,res)

    //variaveis importantes para o fluxo da aplicação
    let documento = new Documento(null, 
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null);

    //buscando documento pelo id para garantir que não houve alteração dos valores
    documento = await api.buscarPorID(req.body.id, req.session.userid)
    
    //recupera a opção de pagamento selecionada pelo usuário
    let opcaoSelecionda = req.body.opcaoPagamento

    console.log("Forma Pagamento Selecionada: " + opcaoSelecionda)

    //recupera as opções oferecidas
    let opcaoPagamento =  documento.opcoesDeCompra.formaPagamento

    //procura nas opções oferecidas as opção selecionada
    let item = opcaoPagamento.find(element => element.id == opcaoSelecionda);

    console.log("Forma Pagamento Selecionada: " + JSON.stringify(item))

    //atribui opção selecinado no documento
    documento.opcoesDeSelecao.formaPagamento.forma = item.forma
    documento.opcoesDeSelecao.formaPagamento.tipo = item.tipo
    documento.opcoesDeSelecao.formaPagamento.parcelas = item.parcelas
    documento.opcoesDeSelecao.formaPagamento.valor = item.valor

    console.log("Documento depois de alterar")
    console.log(documento)

    //chamando controller de calculos
    documento =  await api.emitir(documento, req.session.userid)

    console.log("apos emitir")
    console.log(documento)

    //Simulando a chamando ao legado
    documento =  await api.enviarParaContabilidadeMock(documento, req.session.userid)
    documento =  await api.enviarParaFinanceiro(documento, req.session.userid)

    //chamando controller de dados
    documento =  await api.incluirNovoRegistro(documento, req.session.userid)

    console.log("Documento Interno Apos banco de dados")
    console.log(documento)

    //se estiver logado, direcionar a pagina para o usuário
    if(logado){
        res.render('emissaoPage', documento)
     }else{
        res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
    }
        
};






