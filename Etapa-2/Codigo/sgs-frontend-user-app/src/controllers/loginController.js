/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: Camada de Interface de Usuário - Componente Web Responsivo
 * Autor: Danilo Sgrott
 * Modulo: loginController.js
 * Objetivo: Modulo responsavel por controlar todos os fluxos de login e redirect da página
 */

'use strict';

//Imports de modulos obrigatórios
var path = require( 'path' );
var UAParser = require('ua-parser-js');
var security    = require(path.resolve( 'src/controllers/securityMockController' ));

//Função responsável por validar o login do usuário
exports.login = function(req, res) {

    console.log(req.session)
    console.log(req.headers["user-agent"])

    var parser = new UAParser();
    var ua = req.headers['user-agent'];
    var browserName = parser.setUA(ua).getBrowser().name;

    console.log("browserName: " + browserName)

    if(!browserName.includes("Safari") && !browserName.includes("Chrome")){

        res.render('browserNaoAutorizado', {"mensagem":null})

    }

    //Validando Segurança
    var logado = security.validarAcesso(req,res)

    //se estiver logando direciona para area logada
    if(logado){
        res.render('areaLogada', {"page":"mainPage"})
    }else{
        res.render('login', {"mensagem":null})
    }

    
};


//Função responsável por autenticar o usuário
exports.autenticar = function(req, res) {

    //Validando Segurança
    var logado = security.validarAcesso(req,res)

    console.log(logado)

    //se estiver logando direciona para area logada
    if(logado){
        res.render('areaLogada', {"page":"mainPage"})
    }else{
        res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
    }

};



//Função responsável por carregar a pagina principal
exports.carregarMainPage = function(req, res) {

    //Validando Segurança
    var logado = security.validarAcesso(req,res)

    console.log(logado)

    //se estiver logando direciona para pagina principal
    if(logado){
        res.render('mainPage', {"usuario":req.session.userName})
    }else{
        res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
    }

};

//Função responsável por carregar as paginas de funcionalidades não liberadas
exports.carregarFuncionalidadeNaoLiberada = function(req, res) {

    //Validando Segurança
    var logado = security.validarAcesso(req,res)

    //recupera qual pagina esta sendo requisitada
    let page = req.query.page

    console.log(req.session)

    //seta a mensagem da tela
    let msg = {menssagem:""}

    //cria mensagem padrão para cada página
    switch (page) {
        case "cadprod":
            msg.menssagem = "A funcionalidade de Cadastro de Produto não está implementada."
            break;
        case "endosso":
            msg.menssagem = "A funcionalidade de Emisão de Endossos não está implementada."
            break;
        case "rel":
            msg.menssagem = "A funcionalidade de Relatórios não está implementada."
            break;
        case "conf":
            msg.menssagem = "A funcionalidade de Configurações não está implementada."
            break;
        case "pend":
            msg.menssagem = "A funcionalidade de Pendências não está implementada."
            break;
        case "aprov":
            msg.menssagem = "A funcionalidade de Aprovações não está implementada."
            break;                          
        default:
          msg.menssagem = ""
            break;
      }


    //se estiver logando direciona para pagina de funcionalidade não liberada
    if(logado){
        res.render('funcNaoLiberada', msg)
    }else{
        res.render('login', {"mensagem":"Usuário ou senha invalidos!"})
    }

};

//Função responsável por realizar o logout
exports.logout = function(req, res) {

    //destrou a sessão e direciona para a tela de login
    req.session.destroy();
    res.render('login', {"mensagem":null})
};




