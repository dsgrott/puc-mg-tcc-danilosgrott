/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: API de Regras, Calculo, Consulta e Gravação de Dados Mock
 * Autor: Danilo Sgrott
 * Modulo: securityMockController.js
 * Objetivo: Modulo responsável por mockar todo o fluxo de segurança da aplicação
 */

'use strict';

//Imports de modulos obrigatórios
var path = require( 'path' );

//Função responsável por validar o acesso do usuário
exports.validarAcesso = function(req, res) {

    console.log("Validando acesso")

    //mock de usuário
    var usuario = process.env.ACESSO_USUARIO
    var senha = process.env.ACESSO_PASSWORD
    var nomeUser = process.env.ACESSO_NOME_USUARIO

    //console.log(req.body)

    //recuperar sessão criada
    var session = req.session;

    //validando o acesso e setando a sessão criada retornando true (logado) ou false (deslogado)
    if(req.body.inputUser == usuario && req.body.inputPassword == senha){
        session.userid=usuario;
        session.userName=nomeUser;
        session.logado=true;
        console.log(req.session)
        return true
    } 
    else{
        //console.log(req.session)

        if(session.logado){
            return true
        }else{
            return false
        }
        
    } 
};
 




