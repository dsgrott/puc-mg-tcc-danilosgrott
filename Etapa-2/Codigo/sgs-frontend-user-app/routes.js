
/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: Camada de Interface de Usuário - Componente Web Responsivo
 * Autor: Danilo Sgrott
 * Modulo: routes.js
 * Objetivo: Modulo responsável pelas rotas da aplicação
 */

'use strict'; 

//função responsável por mapear todas as rotas externas da aplicação
module.exports = function(app) {

  //Import de modulos e variaveis importantes
  var path = require( 'path' );
  var login    = require(path.resolve( 'src/controllers/loginController' ));
  var emissao    = require(path.resolve( 'src/controllers/emissaoController' ));
  var pesquisa    = require(path.resolve( 'src/controllers/pesquisaController' ));
  
  //Mapeamento da rota raiz
  app.route('/')
    .get(login.login);
    
  app.route('/login')
    .get(login.login);

  app.route('/logout')
    .get(login.logout);

  app.route('/home')
    .post(login.autenticar)
    .get(login.autenticar);

  app.route('/mainPage') 
    .get(login.carregarMainPage)

  app.route('/notFunc')
    .get(login.carregarFuncionalidadeNaoLiberada)

  app.route('/emisNV')
    .get(emissao.carregarPaginaDeEmissao)

  app.route('/validar')
    .post(emissao.validarProposta)

  app.route('/emitir')
    .post(emissao.emitirProposta)

  app.route('/consulta')
    .get(pesquisa.carregarPaginaDePesquisa)
    .post(pesquisa.redirecionarProposta);


    
};
