/*!
 * SGS - Sistema de Gestão de Seguros
 * Projeto: Camada de Interface de Usuário - Componente Web Responsivo
 * Autor: Danilo Sgrott
 * Modulo: serve.js
 * Objetivo: Modulo inicial da aplição (Main) possui as configurações inicias do server
 */

//Import de modulos e variaveis importantes
const path = require( 'path' );
const express = require('express');
const cookieParser = require("cookie-parser");
const sessions = require('express-session');
const app = express();
const port = process.env.PORT;

//Ajustando Sessao
const oneDay = 24 * 60 * 60 * 1000;
console.log(oneDay)

//iniciando bloco de sessao
app.use(sessions({
    secret: process.env.SESSION_SECRET,
    saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false 
}));

// parsing the incoming data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//serving public file
app.use(express.static(__dirname + '/public'));

// cookie parser middleware
app.use(cookieParser());

//Importando rotas
var routes = require( path.resolve( 'routes' )); 

//registro de rotas
routes(app);  

app.set('view engine', 'ejs');
app.set('views', './src/views');

//Iniciando o listener de porta
app.listen(port);

console.log('Camada de Interface de Usuário - Componente Web Responsivo no ar: ' + port);

//Valida se foi chamado alguma rota inexistente e retorna 404
app.use(function(req, res) {
  res.render('404Page', {})
});