# Entrega do Trabalho de Conclusão de Curso (TCC) - Arquitetura de Software Distribuído - PUC Minas - Danilo Sgrott Santana

## Links úteis das entregas
- Link para acesso público de todos os documento: https://gitlab.com/dsgrott/puc-mg-tcc-danilosgrott.git
- Link para o Video Etapa 3: https://youtu.be/a5aWQcGJYTk
- Link para o Video Etapa 2: https://youtu.be/SZ26q6O08D4
- Link para o Video Etapa 1: https://youtu.be/jb8DUH6BofM

---------------------------------------------------------------------------------------------------------
## Relação dos documentos no repositório GIT - ETAPA 3

**Diretório:** /Etapa-3

**Data da entrega:** 15/10/2022

- TCC-DaniloSgrottSantana-etapa3.docx (Documento editável da entrega final do TCC)
- TCC-DaniloSgrottSantana-etapa3.pdf (Doducumento fechado em PDF da entrega final do TCC)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA3.key (Apresentação editável Keynotes do video de apresentação)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA3.pdf (Apresentação fechada e completa da apresentação do video)
- VIDEO-TTC-APRESENTACAO-ETAPA2.mp4 (Video de apresentação em formato mp4)

## Documentos uploaded no Portal PUC
- TCC-DaniloSgrottSantana-etapa3.pdf (Doducumento fechado em PDF da entrega final do TCC)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA3.pdf(Apresentação fechada e completa da apresentação do video)
- VIDEO-TTC-APRESENTACAO-ETAPA3.mp4 (Video de apresentação em formato mp4)
- README.md (Este documento)

## Etapas pendentes e previsão de entrega
- Não exitem mais etapas pendentes

---------------------------------------------------------------------------------------------------------
## Relação dos documentos no repositório GIT - ETAPA 2

**Diretório:** /Etapa-2

**Data da entrega:** 15/06/2022

- Codigo/sgs-frontend-user-app (Codigo da aplicação Front-end)
- Codigo/sgs-backend-api-mock (Códido da Aplicação Back-end)
- Diagramas (Diretório com todos os diagramas prudizidos nessa etapa do trabalho)
- Wireframe (Diretório com todos os wireframes prudizidos nessa etapa do trabalho)
- TCC-DaniloSgrottSantana-etapa2.docx (Documento editável da entrega final do TCC)
- TCC-DaniloSgrottSantana-etapa2.pdf (Doducumento fechado em PDF da entrega final do TCC)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA2.key (Apresentação editável Keynotes do video de apresentação)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA2.pdf (Apresentação fechada e completa da apresentação do video)
- VIDEO-TTC-APRESENTACAO-ETAPA2.mp4 (Video de apresentação em formato mp4)

## Documentos uploaded no Portal PUC
- TCC-DaniloSgrottSantana-etapa2.pdf (Doducumento fechado em PDF da entrega final do TCC)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA2.pdf(Apresentação fechada e completa da apresentação do video)
- VIDEO-TTC-APRESENTACAO-ETAPA2.mp4 (Video de apresentação em formato mp4)
- README.md (Este documento)

---------------------------------------------------------------------------------------------------------
## Relação dos documentos no repositório GIT - ETAPA 1

**Diretório:** /Etapa-1

**Data da entrega:** 15/04/2022

- c4-contexto.jpg (Imagem individual do diagrama de contexto)
- TCC-DaniloSgrottSantana-Documento-Etapa1.docx (Documento editável da entrega final do TCC)
- TCC-DaniloSgrottSantana-Documento-Etapa1.pdf (Doducumento fechado em PDF da entrega final do TCC)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA1.key (Apresentação editável Keynotes do video de apresentação)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA1.pdf (Apresentação fechada e completa da apresentação do video)
- VIDEO-TTC-APRESENTACAO-ETAPA1.mp4 (Video de apresentação em formato mp4)
- VIDEO-TTC-APRESENTACAO-ETAPA1.mv4 (Video de apresentação em formato mv4)

## Documentos uploaded no Portal PUC
- TCC-DaniloSgrottSantana-Documento-Etapa1.pdf (Doducumento fechado em PDF da entrega final do TCC)
- TTC-PUC-MINAS-APRESENTACAO-ETAPA1.pdf (Apresentação fechada e completa da apresentação do video)
- VIDEO-TTC-APRESENTACAO-ETAPA1.mp4 (Video de apresentação em formato mp4)
- README.md (Este documento)
---------------------------------------------------------------------------------------------------------